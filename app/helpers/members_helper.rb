module MembersHelper
  def contar_pontos(membro)
    pos = 0
    neg = 0
    membro.gamefications.each do |g|
      if g.positive?
        pos += g.points
      else
        neg += g.points
      end
    end
    return [pos, neg]
  end

  def dessasociar_projeto(project_id)
    link_to member_remove_assoc_path(:project_id => project_id),
     data: { confirm: 'Are you sure?' }, method: :put do |f|
      content_tag(:i, "", class: "fa fa-close")
    end
  end

  def dessasociar_game(gamefication_id)
    link_to member_remove_assoc_path(:gamefication_id => gamefication_id),
     data: { confirm: 'Are you sure?' }, method: :put do |f|
      content_tag(:i, "", class: "fa fa-close")
     end
  end
end
