class Member < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :rememberable, :validatable

  has_and_belongs_to_many :projects
  has_many :gamefications, :dependent => :destroy
  mount_uploader :avatar, AvatarUploader
end
