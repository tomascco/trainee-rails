class Gamefication < ApplicationRecord
  belongs_to :member
  validates :points, numericality: {greater_than: 0, message: "para pontos negativos marque a opção"}
end
