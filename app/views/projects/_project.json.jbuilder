json.extract! project, :id, :name, :members_id, :start, :finish, :created_at, :updated_at
json.url project_url(project, format: :json)
