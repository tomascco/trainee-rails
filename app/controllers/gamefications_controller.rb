class GameficationsController < ApplicationController
  before_action :set_gamefication, only: [:edit, :update, :destroy]
  before_action :authenticate!
  before_action :authenticate_admin!, only: [:edit, :update, :destroy, :create, :new]

  # GET /gamefications/new
  def new
    @gamefication = Gamefication.new
  end

  # GET /gamefications/1/edit
  def edit
  end

  # POST /gamefications
  # POST /gamefications.json
  def create
    @gamefication = Gamefication.new(gamefication_params)

    respond_to do |format|
      if @gamefication.save
        format.html { redirect_to member_path(@gamefication.member_id), notice: 'Gamefication was successfully created.' }
      else
        format.html { render :new }
      end
    end
  end

  # PATCH/PUT /gamefications/1
  # PATCH/PUT /gamefications/1.json
  def update
    respond_to do |format|
      if @gamefication.update(gamefication_params)
        format.html { redirect_to member_path(@gamefication.member_id), notice: 'Gamefication was successfully updated.' }
      else
        format.html { render :edit }
      end
    end
  end

  # DELETE /gamefications/1
  # DELETE /gamefications/1.json
  def destroy
    redirect = @gamefication.member_id
    @gamefication.destroy
    respond_to do |format|
      format.html { redirect_to member_path(redirect), notice: 'Gamefication was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_gamefication
      @gamefication = Gamefication.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def gamefication_params
      params.require(:gamefication).permit(:member_id, :reason, :points, :positive)
    end
end
