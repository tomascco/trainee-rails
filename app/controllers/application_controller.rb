class ApplicationController < ActionController::Base
  layout :layout
  def authenticate!
    if member_signed_in?
      true
    elsif admin_signed_in?
      true
    else
      authenticate_member!
    end
  end
  private
    def layout
      # only turn it off for login pages:
      is_a?(Devise::SessionsController) ? "login" : "admin_lte_2"
      # or turn layout off for every devise controller:
      #devise_controller? && "application"
    end
end
