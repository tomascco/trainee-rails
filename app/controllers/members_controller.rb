class MembersController < ApplicationController
  before_action :authenticate_admin!, only: [:create, :remove_assoc]
  before_action :authenticate!
  before_action :set_member, only: [:show, :edit, :update, :destroy, :remove_assoc ]
  require 'carrierwave/orm/activerecord'
  def index
    @membros = Member.all
  end

  def create
    @membro = Member.new(member_params.except(:project_id))
    respond_to do |format|
      if @membro.save
        format.html { redirect_to members_path, notice: 'Deu certo' }
      else
        format.html { render :new }
      end
    end
  end

  def edit
  end

  def new
    @membro = Member.new
  end

  def update
    respond_to do |format|
      if @membro.update(member_params.except(:project_id))
        format.html { redirect_to members_path, notice: 'Deu certo' }
      else
        format.html { render :edit }
      end
    end
  end

  def destroy
    @membro.destroy
    respond_to do |format|
      format.html { redirect_to members_url, notice: 'Deu certo' }
    end
  end

  def remove_assoc
    sucesso = true
    if params[:project_id] != nil
      project = Project.find(params[:project_id])
      @membro.projects.delete(project)
    elsif params[:gamefication_id] != nil
      gamefication = Gamefication.find(params[:gamefication_id])
      @membro.gamefications.delete(gamefication)
    else
      sucesso = false
    end
    if sucesso
      respond_to do |format|
        format.html {redirect_to member_path(@membro), notice: 'Deletado'}
      end
    else
      respond_to do |format|
        format.html {redirect_to member_path(@membro), notice: '???'}
      end
    end
  end

  private

    def set_member
      @membro = Member.find(params[:id])
    end

    def member_params
      if params[:member][:password] == ''
        params.require(:member).permit(:name, :email, :project_id, :gamefication_id, :avatar)
      else
        params.require(:member).permit(:name, :email, :project_id, :password, :gamefication_id, :avatar)
      end
    end
  end
