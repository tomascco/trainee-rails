class CreateGamefications < ActiveRecord::Migration[5.2]
  def change
    create_table :gamefications do |t|
      t.references :member, foreign_key: true
      t.string :reason
      t.integer :points
      t.boolean :positive

      t.timestamps
    end
  end
end
