class AddReferencesToMember < ActiveRecord::Migration[5.2]
  def change
    add_reference :members, :gamefication, foreign_key: true
    create_join_table :members, :projects
  end
end
