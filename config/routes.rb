Rails.application.routes.draw do
  root 'members#index'
  resources :gamefications, only: [:new, :create, :destroy, :edit, :update]
  resources :projects, except: [:show]
  get '/projects/:id/allocate', to: 'projects#allocate', as: 'allocate_project'
  post '/projects/:id/allocate', to: 'projects#associate', as: 'associate_project'
  delete '/projects/:id/allocate', to: 'projects#dessociate', as: 'dessociate_project'
  resources :members
  put '/members/:id/remove_assoc(.:format)', to: 'members#remove_assoc', as: 'member_remove_assoc'
  devise_for :members, :path => "/member" ,:skip => [:registrations]
  devise_scope :member do
    get "/member", to: "devise/sessions#new"
  end
  devise_for :admins, :path => "/admin", :skip => [:registrations]
  devise_scope :admin do
    get "/admin", to: "devise/sessions#new"
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
