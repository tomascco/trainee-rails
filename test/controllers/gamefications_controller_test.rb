require 'test_helper'

class GameficationsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @gamefication = gamefications(:one)
  end

  test "should get index" do
    get gamefications_url
    assert_response :success
  end

  test "should get new" do
    get new_gamefication_url
    assert_response :success
  end

  test "should create gamefication" do
    assert_difference('Gamefication.count') do
      post gamefications_url, params: { gamefication: { member_id: @gamefication.member_id, negative: @gamefication.negative, positive: @gamefication.positive, reason: @gamefication.reason } }
    end

    assert_redirected_to gamefication_url(Gamefication.last)
  end

  test "should show gamefication" do
    get gamefication_url(@gamefication)
    assert_response :success
  end

  test "should get edit" do
    get edit_gamefication_url(@gamefication)
    assert_response :success
  end

  test "should update gamefication" do
    patch gamefication_url(@gamefication), params: { gamefication: { member_id: @gamefication.member_id, negative: @gamefication.negative, positive: @gamefication.positive, reason: @gamefication.reason } }
    assert_redirected_to gamefication_url(@gamefication)
  end

  test "should destroy gamefication" do
    assert_difference('Gamefication.count', -1) do
      delete gamefication_url(@gamefication)
    end

    assert_redirected_to gamefications_url
  end
end
