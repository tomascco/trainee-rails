require 'test_helper'

class ManageMembersControllerTest < ActionDispatch::IntegrationTest
  test "should get show" do
    get manage_members_show_url
    assert_response :success
  end

  test "should get create" do
    get manage_members_create_url
    assert_response :success
  end

  test "should get edit" do
    get manage_members_edit_url
    assert_response :success
  end

end
