require "application_system_test_case"

class GameficationsTest < ApplicationSystemTestCase
  setup do
    @gamefication = gamefications(:one)
  end

  test "visiting the index" do
    visit gamefications_url
    assert_selector "h1", text: "Gamefications"
  end

  test "creating a Gamefication" do
    visit gamefications_url
    click_on "New Gamefication"

    fill_in "Member", with: @gamefication.member_id
    fill_in "Negative", with: @gamefication.negative
    fill_in "Positive", with: @gamefication.positive
    fill_in "Reason", with: @gamefication.reason
    click_on "Create Gamefication"

    assert_text "Gamefication was successfully created"
    click_on "Back"
  end

  test "updating a Gamefication" do
    visit gamefications_url
    click_on "Edit", match: :first

    fill_in "Member", with: @gamefication.member_id
    fill_in "Negative", with: @gamefication.negative
    fill_in "Positive", with: @gamefication.positive
    fill_in "Reason", with: @gamefication.reason
    click_on "Update Gamefication"

    assert_text "Gamefication was successfully updated"
    click_on "Back"
  end

  test "destroying a Gamefication" do
    visit gamefications_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Gamefication was successfully destroyed"
  end
end
